{
  stdenv,
  lib,
  buildGoModule,
  fetchFromGitHub,
  spotify,
  spicetify-cli,
  theme,
  colorscheme,
  writeShellScript,
  writeText,
  python312Packages,
  imagemagick,
  jc,
  jq,
}:
let
  inherit (python312Packages) icnsutil;
  spicetify-unwrapped = stdenv.mkDerivation {
    pname = "spicetify-unwrapped";
    inherit (spotify) version;
    src = spotify;

    buildPhase = let
      buildPre =
        if stdenv.isDarwin
        then ''
          mkdir -p $(pwd)/spicetify-config/Library/Application\ Support/Spotify
          mkdir -p $(pwd)/spicetify-config/.config/spicetify/
          export SPICETIFY_DIR="$(pwd)/spicetify-config/.config/spicetify"
          export SPOTIFY_CONTENTS_DIR="$(pwd)/Applications/Spotify.app/Contents/Resources"
        ''
        else ''
          mkdir -p "$(pwd)/spicetify-config"
          touch "$(pwd)/spicetify-config/prefs"
          export SPICETIFY_DIR="$(pwd)/spicetify-config/spicetify"
          export SPOTIFY_CONTENTS_DIR="$(pwd)/share/spotify"
        '';
      buildPost =
        if stdenv.isDarwin
        then ''
          mkdir -p $out/lib
          cp -r Applications $out/lib
          mkdir -p $out/Applications/Spotify.app/Contents/MacOS
        ''
        else ''
          # Patch spotify wrapper
          sed -i "s#${spotify}#$out#g" ./bin/spotify

          cp -r bin lib share $out
        '';
    in ''
      ${buildPre}

      mkdir -p $(pwd)/spicetify-config
      touch $(pwd)/spicetify-config/prefs

      export HOME=$(pwd)/spicetify-config
      export XDG_CONFIG_HOME=$(pwd)/spicetify-config

      CFG_PATH=$(${spicetify-cli}/bin/spicetify-cli -c)
      ${spicetify-cli}/bin/spicetify-cli config 2>&1 > /dev/null || true

      sed -i "s:^spotify_path.*:spotify_path = $SPOTIFY_CONTENTS_DIR:" $CFG_PATH
      sed -i "s:^prefs_path.*:prefs_path = $(pwd)/spicetify-config/prefs:" $CFG_PATH

      mkdir -p "$SPICETIFY_DIR/Themes"
      cp -R ${theme} "$SPICETIFY_DIR/Themes/"

      ${spicetify-cli}/bin/spicetify-cli config current_theme ${builtins.baseNameOf theme}
      ${spicetify-cli}/bin/spicetify-cli config color_scheme ${colorscheme}
      cat $(${spicetify-cli}/bin/spicetify-cli -c)
      ${spicetify-cli}/bin/spicetify-cli backup apply || true
      ${spicetify-cli}/bin/spicetify-cli apply

      mkdir -p $out/bin
      ${buildPost}
    '';
  };
  darwinWrapper = writeShellScript "spotify" ''
    echo "Disabling Spotify updates"
    updateDir="$HOME/Library/Application Support/Spotify/PersistentCache/Update"
    mkdir -p "$updateDir"
    /usr/bin/chflags -R uchg "$updateDir"
    ${spicetify-unwrapped.outPath}/lib/Applications/Spotify.app/Contents/MacOS/Spotify
    #/usr/bin/chflags -R nouchg "$updateDir" && echo "Re-enabled updates"
  '';
  infoPlist = writeText "Info.plist" ''
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
    <dict>
      <key>CFBundleDevelopmentRegion</key>
      <string>English</string>
      <key>CFBundleExecutable</key>
      <string>Spotify</string>
      <key>CFBundleIconFile</key>
      <string>Icon.icns</string>
    </dict>
    </plist>
  '';
in
  stdenv.mkDerivation {
    pname = "spotify";
    src = spicetify-unwrapped;
    inherit (spicetify-unwrapped) version;

    buildPhase = if stdenv.isDarwin then ''      
      mkdir -p $out/lib $out/bin
      cp -r ${spicetify-unwrapped.outPath}/lib/Applications $out/lib
      cp ${darwinWrapper} $out/bin/spotify
      # Dummy MacOS "app" to always launch the wrapper
      mkdir -p $out/Applications/Spotify.app/Contents/MacOS/
      cp ${infoPlist} $out/Applications/Spotify.app/Contents/Info.plist
      mkdir -pv $out/Applications/Spotify.app/Contents/Resources
      echo 'APPL????' > $out/Applications/Spotify.app/Contents/PkgInfo
      ln -s $out/bin/spotify $out/Applications/Spotify.app/Contents/MacOS/Spotify

      ICON_COLOR="$(cat ${theme}/color.ini | ${jc}/bin/jc --ini | tr 'A-Z' 'a-z' | ${jq}/bin/jq -r '."${colorscheme}".text' | sed 's/ //g' | sed 's/#//g' | sed 's/^/#/')"   
      export SPOTIFY_RESOURCES_DIR="${spotify}/Applications/Spotify.app/Contents/Resources"
      cp $SPOTIFY_RESOURCES_DIR/Icon.icns ./Icon.icns
      ${icnsutil}/bin/icnsutil e Icon.icns
      cp ./Icon.icns.export/512x512@2x.png icon.png
      ${imagemagick}/bin/magick icon.png -fuzz 40% -fill "$ICON_COLOR" -opaque '#1CD760' modified.png
      ${icnsutil}/bin/icnsutil c modified.icns modified.png
      cp modified.icns $out/Applications/Spotify.app/Contents/Resources/Icon.icns
    '' else ''
      mkdir -p $out
      cp -r ${spicetify-unwrapped.outPath}/* $out
    '';
  }
