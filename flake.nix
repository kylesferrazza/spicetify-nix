{
  description = "spicetify nix";

  inputs = {
    spicetify-themes = {
      url = "github:spicetify/spicetify-themes";
      flake = false;
    };
    spicetify-themes-alindl = {
      url = "github:alindl/spicetify-themes";
      flake = false;
    };
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    spicetify-themes,
    spicetify-themes-alindl,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = import nixpkgs {
          inherit system;
          config.allowUnfree = true;
          config.allowAliases = true;
        };
        mkSpotify = {theme, colorscheme, repo ? spicetify-themes}: pkgs.callPackage ./spicetify.nix {
          theme = "${repo}/${theme}";
          inherit colorscheme;
        };
      in {
        formatter = pkgs.alejandra;
        packages = {
          dracula = mkSpotify {
            theme = "Sleek";
            colorscheme = "dracula";
          };
          dracula-text = mkSpotify {
            theme = "text";
            colorscheme = "dracula";
          };
          nord = mkSpotify {
            theme = "Sleek";
            colorscheme = "nord";
          };
          nord-text = mkSpotify {
            theme = "text";
            colorscheme = "nord";
          };
          solarizedDark = mkSpotify {
            theme = "Ziro";
            colorscheme = "solarized-dark";
            repo = spicetify-themes-alindl;
          };
          solarized-text = mkSpotify {
            theme = "text";
            colorscheme = "solarized";
          };
        };
      }
    );
}
